<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>PasarBogor</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>f1d3f785-1b44-401c-b868-fedc57ad872c</testSuiteGuid>
   <testCaseLink>
      <guid>651aa4b8-e350-4e29-afa8-6a49ab78863f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PasarBogor/Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2161cebf-1bd5-49a2-bdf7-eb737210f450</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PasarBogor/Tambah Pedagang</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2f1d8077-4058-4e8f-bc22-d112fc6fc23c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PasarBogor/Cari-Lihat-Ubah-Hapus Pedagang</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>99e0a402-4998-4f7b-931c-6f9adb234daf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PasarBogor/Tambah Lapak</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dc993b81-73ef-4e20-85da-34b2b70c8f11</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PasarBogor/Cari-Lihat-Ubah-Hapus Lapak</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>288ea031-efcb-430d-bcaf-ea8abd977712</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PasarBogor/Tambah Produk</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5b60e990-58ba-4ae4-8415-ae0e6b72f9b1</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3ee6ada4-bfbb-4194-a074-96b5f6dee0a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PasarBogor/Logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
