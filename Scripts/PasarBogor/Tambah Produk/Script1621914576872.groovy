import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.apache.commons.lang.RandomStringUtils as RandStr

WebUI.click(findTestObject('PasarBogor/Dashboard/menu_daftarproduk'))

WebUI.click(findTestObject('PasarBogor/Produk/btn_tambahproduk'))

WebUI.click(findTestObject('PasarBogor/rec/span_-- Pilih salah satu --'))

WebUI.setText(findTestObject('PasarBogor/rec/input_BRI  2021_kt-loop-input'), 'Lapak Beras 01')

WebUI.sendKeys(findTestObject('PasarBogor/rec/input_BRI  2021_kt-loop-input'), Keys.chord(Keys.ENTER))

WebUI.delay(3)

rand_number = RandStr.randomNumeric(3)

WebUI.setText(findTestObject('PasarBogor/rec/input__txt_nama'), namaproduk + rand_number)

WebUI.setText(findTestObject('PasarBogor/rec/input__dd_satuan'), 'kg')

WebUI.setText(findTestObject('PasarBogor/rec/input__txt_stok'), '20')

WebUI.setText(findTestObject('PasarBogor/rec/input_Rp_txt_harga'), '60000')

WebUI.setText(findTestObject('PasarBogor/rec/textarea__txt_deskripsi'), 'Beras pulen dan wangi')

WebUI.scrollToElement(findTestObject('PasarBogor/rec/button_Simpan'), 30)

WebUI.click(findTestObject('PasarBogor/rec/button_Simpan'))

WebUI.delay(5)

