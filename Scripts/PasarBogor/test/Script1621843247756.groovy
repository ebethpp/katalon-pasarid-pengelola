import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://pengelola-pasar.tcd-dev.id/')

WebUI.click(findTestObject('PasarBogor/rec/input_Username_txt_username'))

WebUI.setText(findTestObject('PasarBogor/rec/input_Username_txt_username'), 'pasarbogorid')

WebUI.setEncryptedText(findTestObject('PasarBogor/rec/input_Username_txt_password'), 'iFGeFYmXIrU6ruIopQUS+w==')

WebUI.click(findTestObject('PasarBogor/rec/button_LOG IN'))

WebUI.click(findTestObject('PasarBogor/rec/a_Daftar Lapak'))

WebUI.click(findTestObject('PasarBogor/rec/a_tambah lapak'))

WebUI.setText(findTestObject('PasarBogor/rec/input__txt_nama_lapak'), 'Lapak Beras 111')

WebUI.setText(findTestObject('PasarBogor/rec/input__txt_kode_lapak'), 'Beras 111')

WebUI.setText(findTestObject('PasarBogor/rec/textarea__txt_deskripsi'), 'Menjual beras')

WebUI.click(findTestObject('PasarBogor/rec/span_-- Pilih salah satu --'))

WebUI.setText(findTestObject('PasarBogor/rec/input_concat(id(, , select2-dd_pedagang-con_0da18e'), '081313215010')

WebUI.sendKeys(findTestObject('PasarBogor/rec/input_concat(id(, , select2-dd_pedagang-con_0da18e'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('PasarBogor/rec/ul_sembako'))

WebUI.click(findTestObject('PasarBogor/rec/span_'))

WebUI.setText(findTestObject('PasarBogor/rec/input_sembako_kt-loop-input'), 'sembako')

WebUI.click(findTestObject('PasarBogor/rec/span_'))

WebUI.click(findTestObject('PasarBogor/rec/div_Kategori -- Pilih kategori --buah-buaha_8e7700'))

WebUI.click(findTestObject('PasarBogor/rec/span_'))

WebUI.click(findTestObject('PasarBogor/rec/a_pasar bogor'))

WebUI.rightClick(findTestObject('PasarBogor/rec/li_Kode Lapak wajib diisi dan harus berupa _978797'))

WebUI.click(findTestObject('PasarBogor/rec/a_Logout'))

WebUI.click(findTestObject('PasarBogor/rec/button_Batal'))

WebUI.click(findTestObject('PasarBogor/rec/a_pasar bogor'))

WebUI.click(findTestObject('PasarBogor/rec/a_Logout'))

WebUI.click(findTestObject('PasarBogor/rec/button_Batal'))

