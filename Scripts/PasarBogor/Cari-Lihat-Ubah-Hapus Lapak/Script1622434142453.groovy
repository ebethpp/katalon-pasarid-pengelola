import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.apache.commons.lang.RandomStringUtils as RandStr

WebUI.click(findTestObject('PasarBogor/Dashboard/menu_daftarlapak'))

WebUI.setText(findTestObject('PasarBogor/Lapak/field_carilapak'), GlobalVariable.carilapak)

WebUI.click(findTestObject('PasarBogor/Lapak/btn_detaillapak'))

WebUI.delay(3)

WebUI.back()

WebUI.click(findTestObject('PasarBogor/Dashboard/menu_daftarlapak'))

WebUI.setText(findTestObject('PasarBogor/Lapak/field_carilapak'), GlobalVariable.carilapak)

WebUI.click(findTestObject('PasarBogor/Lapak/btn_ubahlapak'))

rand_alpha = RandStr.randomAlphabetic(3)

WebUI.setText(findTestObject('PasarBogor/Lapak/field_deskripsi'), GlobalVariable.deskripsilapak + rand_alpha)

WebUI.scrollToElement(findTestObject('PasarBogor/Lapak/btn_submit'), 30)

WebUI.click(findTestObject('PasarBogor/Lapak/btn_submit'))

WebUI.setText(findTestObject('PasarBogor/Lapak/field_carilapak'), GlobalVariable.carilapak)

WebUI.click(findTestObject('PasarBogor/Lapak/btn_hapuslapak'))

WebUI.click(findTestObject('PasarBogor/Lapak/modal_batal'))

