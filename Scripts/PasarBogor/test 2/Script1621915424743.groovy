import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://pengelola-pasar.tcd-dev.id/')

WebUI.setText(findTestObject('PasarBogor/rec/input_Username_txt_username'), 'pasarbogorid')

WebUI.setEncryptedText(findTestObject('PasarBogor/rec/input_Username_txt_password'), 'iFGeFYmXIrU6ruIopQUS+w==')

WebUI.click(findTestObject('PasarBogor/rec/button_LOG IN'))

WebUI.click(findTestObject('PasarBogor/rec/a_Daftar Produk'))

WebUI.click(findTestObject('PasarBogor/rec/a_Tambah Produk'))

WebUI.click(findTestObject('PasarBogor/rec/span_-- Pilih salah satu --'))

WebUI.setText(findTestObject('PasarBogor/rec/input_BRI  2021_kt-loop-input'), 'lapak beras')

WebUI.sendKeys(findTestObject('PasarBogor/rec/input_BRI  2021_kt-loop-input'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('PasarBogor/rec/input__txt_nama'), 'Beras Putih')

WebUI.setText(findTestObject('PasarBogor/rec/input__dd_satuan'), 'kg')

WebUI.setText(findTestObject('PasarBogor/rec/input__txt_stok'), '20')

WebUI.setText(findTestObject('PasarBogor/rec/input_Rp_txt_harga'), '60000')

WebUI.setText(findTestObject('PasarBogor/rec/textarea__txt_deskripsi'), 'beras pulen')

WebUI.click(findTestObject('PasarBogor/rec/button_Simpan'))

WebUI.click(findTestObject('PasarBogor/rec/strong_pasar bogor'))

WebUI.click(findTestObject('PasarBogor/rec/a_Logout'))

WebUI.click(findTestObject('PasarBogor/rec/a_Logout_1'))

WebUI.closeBrowser()

