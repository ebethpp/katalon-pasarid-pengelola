import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.apache.commons.lang.RandomStringUtils as RandStr
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

WebUI.click(findTestObject('PasarBogor/Dashboard/menu_daftarlapak'))

WebUI.click(findTestObject('PasarBogor/Lapak/btn_tambahlapak'))

rand_number = RandStr.randomNumeric(3)

WebUI.setText(findTestObject('PasarBogor/Lapak/field_nama'), GlobalVariable.namalapak + rand_number)

WebUI.setText(findTestObject('PasarBogor/Lapak/field_kodelapak'), GlobalVariable.kodelapak + rand_number)

WebUI.setText(findTestObject('PasarBogor/Lapak/field_deskripsi'), 'Menjual berbagai jenis beras')

WebUI.click(findTestObject('PasarBogor/Lapak/field_pilihpedagang'), FailureHandling.STOP_ON_FAILURE)

//WebUI.setText(log.getTestObjectWithXpath('//*[@id="kt-loop-input"])[3]'), '081313215010')
WebUI.setText(findTestObject('PasarBogor/Lapak/field_isipedagang'), '081313215010')

WebUI.sendKeys(findTestObject('PasarBogor/Lapak/field_isipedagang'), Keys.chord(Keys.ENTER))

//WebUI.setText(findTestObject('PasarBogor/Lapak/field_namapedagang'), 'Andre')
//WebUI.click(findTestObject('PasarBogor/rec/span_Andre (081313215010)'))
WebUI.click(findTestObject('PasarBogor/Lapak/field_pilihkategori'))

WebUI.setText(findTestObject('PasarBogor/rec/input_sembako_kt-loop-input'), 'sembako')

WebUI.sendKeys(findTestObject('PasarBogor/rec/input_sembako_kt-loop-input'), Keys.chord(Keys.ENTER))

WebUI.uploadFile(findTestObject('PasarBogor/Lapak/upload_img'), '/mnt/img/beras.jpg', FailureHandling.OPTIONAL)

WebUI.uploadFile(findTestObject('PasarBogor/Lapak/upload_img'), 'D:\\beras.jpg', FailureHandling.OPTIONAL)

WebUI.scrollToElement(findTestObject('PasarBogor/Lapak/btn_submit'), 30)

WebUI.click(findTestObject('PasarBogor/Lapak/btn_submit'))

