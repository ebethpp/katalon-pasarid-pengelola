import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.apache.commons.lang.RandomStringUtils as RandStr

WebUI.click(findTestObject('PasarBogor/Dashboard/menu_daftarpedagang'))

WebUI.setText(findTestObject('PasarBogor/Pedagang/field_caripedagang'), GlobalVariable.caripedagang)

WebUI.click(findTestObject('PasarBogor/Pedagang/btn_detailpedagang'))

WebUI.delay(3)

WebUI.back()

WebUI.click(findTestObject('PasarBogor/Pedagang/btn_ubahpedagang'))

rand_number = RandStr.randomNumeric(3)

WebUI.setText(findTestObject('PasarBogor/Pedagang/field_norek'), GlobalVariable.norek + rand_number)

WebUI.click(findTestObject('PasarBogor/Pedagang/btn_submit'))

WebUI.click(findTestObject('PasarBogor/Dashboard/menu_daftarpedagang'))

WebUI.setText(findTestObject('PasarBogor/Pedagang/field_caripedagang'), GlobalVariable.caripedagang)

WebUI.click(findTestObject('PasarBogor/Pedagang/btn_hapuspedagang'))

WebUI.click(findTestObject('PasarBogor/Pedagang/modal_batal'))

